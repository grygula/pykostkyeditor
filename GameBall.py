import pygame


from GameObject import GameObject

class GameBall(GameObject):
    def __init__(self, s,pos,isMenu):
        super(GameBall, self).__init__(s)
        self.radius =1
        self.stdColor = ((255,199,20,100))
        self.position = pos
        self.isMenu=isMenu
    
    def draw(self):
        if(self.isMenu==1):
            pygame.draw.circle(self.screen, self.getColor(), (self.position[0],self.SCREEN_HEIGHT - self.position[1]),1)
            self.rect =pygame.draw.circle(self.screen, self.getColor(), (self.position[0],self.SCREEN_HEIGHT - self.position[1]),10,2)
        else:
            pygame.draw.circle(self.screen, self.getColor(), (self.position[0],self.SCREEN_HEIGHT - self.position[1]),10)
            self.rect =pygame.draw.circle(self.screen, self.getColor(), (self.position[0],self.SCREEN_HEIGHT - self.position[1]),100,2)
    
