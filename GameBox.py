import pygame
from pygame import Rect

from GameObject import GameObject

class GameBox(GameObject):
    def __init__(self, s,pos):
        super(GameBox, self).__init__(s)
        self.stdColor = (255,199,20,100)
        self.halfWidth =self.halfHight=10
        self.position = pos
        
    def draw(self):
        r = Rect(self.position[0]-self.halfWidth,self.SCREEN_HEIGHT-(self.position[1]+self.halfHight),self.halfWidth*2,self.halfHight*2) 
        self.rect =pygame.draw.rect(self.screen, self.getColor(), r)

    def update(self, dt):
        pass       

    def remove(self):
        pass