import pygame, os
class GameObject(object):
    def __init__(self, s):
        self.screen = s
        self.position = (0, 0)
        self.stdColor = (0, 255, 255, 255)
        self.selectedColor = (0, 0, 0, 0)
        self.PPM=10
        self.SCREEN_HEIGHT=480
        self.rect =None
        self.isSelected=0
    
    def setPosition(self, pos):
        x = pos[0]
        y=self.SCREEN_HEIGHT-pos[1]
        self.position = (x,y)

    def getPosition(self):
        return self.position
    
    def getJsonPosition(self):
        return (self.position[0]/10,self.position[1]/10)
    
    def setColor(self, col):
        self.color = col

    def draw(self):
        pass
    def getColor(self):
        if(self.isSelected==1):
            return self.selectedColor
        else:
            return self.stdColor
    def update(self, dt):
        pass

    def loadImage (self,name):
        name = os.path.join('img', name)
        try:
               image = pygame.image.load(name)
               if image.get_alpha is None:
                        image = image.convert()
               else:
                        image = image.convert_alpha()
        except pygame.error, message:
                print 'Cannot load image:', name
                raise SystemExit, message
        return image, image.get_rect()