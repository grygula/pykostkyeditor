import pygame
from pygame import Rect

from GameObject import GameObject

class SaveButton(GameObject):
    def __init__(self, s,pos):
        super(SaveButton, self).__init__(s)
        self.stdColor = (0,0,255)
        self.halfWidth =self.halfHight=10
        self.position = pos
        self.image, self.rect = self.loadImage('save.png')
    def draw(self):
        self.rect = self.screen.blit(self.image,(self.position[0],self.SCREEN_HEIGHT - self.position[1]))
