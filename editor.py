import pygame
from pygame.locals import *
from GameBox import GameBox
from GamePlatform import GamePlatform
from GameBall import GameBall
from DeleteButton import DeleteButton
from SaveButton import SaveButton
from ExitButton import ExitButton
from json.tests.test_pass1 import JSON
import json

try:
    import android
except ImportError:
    android = None
FPS = 60
class Editor(object):
    def __init__(self, s, boxsJson,padJson,platformJson,level):
        self.level = level #current level
        self.isLoop = 1
        self.screen = s
        self.clock = pygame.time.Clock()
        self.hasBall = False
        self.selectedMenuObject = None
        self.selectedObject= None
        print(padJson)
        print(boxsJson)
        print(platformJson)
        #lets load data if we have them (for edits)
        self.levelObjects = []
        if(padJson!=None):
            self.levelObjects.append(GameBall(self.screen, (padJson[0]*10,padJson[1]*10), 0))
            self.hasBall=True
        if(boxsJson!=None):
            for bjson in boxsJson:
                self.levelObjects.append(GameBox(self.screen, (bjson[0]*10,bjson[1]*10)))

        if(platformJson!=None):
            for pjson in platformJson:
                self.levelObjects.append(GamePlatform(self.screen, (pjson[0]*10,pjson[1]*10)))
        #lets set menu    
        self.menuObjects = []
        self.menuObjects.append(GameBox(self.screen, (780, 300)))
        self.menuObjects.append(GamePlatform(self.screen, (770, 350)))
        self.menuObjects.append(GameBall(self.screen, (780, 400), 1))
        self.specialObjects = []
        self.specialObjects.append(DeleteButton(self.screen, (755, 50)))
        self.specialObjects.append(SaveButton(self.screen, (755, 90)))
        self.specialObjects.append(ExitButton(self.screen, (755, 130)))
    def getSaveJson(self):

        boxes=[]
        platforms=[]
        strjs = '{"name": "Level '+str(self.level+1)+'"'
        for obj in self.levelObjects:
            if(isinstance(obj,GameBall )):
                pad=obj.getJsonPosition()
                strjs +=',"pad": {"y": "'+str(pad[1])+'","x":"'+str(pad[0])+'"}'
            if(isinstance(obj, GameBox)):
                boxes.append(obj.getJsonPosition())
            if(isinstance(obj, GamePlatform)):
                platforms.append(obj.getJsonPosition())
        
        

        strjs +=',"bxs":['
        z=0
        for b in boxes:
            z=z+1
            strjs +='{"y": "'+str(b[1])+'","x": "'+str(b[0])+'"}'
            if(z<len(boxes)):
                strjs +=','
        strjs +=']'

        strjs +=',"platforms":['
        zp=0
        for p in platforms:
            zp=zp+1
            strjs +='{"y": "'+str(p[1])+'","x": "'+str(p[0])+'"}'
            if(zp<len(platforms)):
                strjs +=','
        strjs +=']}'
        return json.loads(strjs)            
    def renderMenu(self):
        for obj in self.menuObjects:
            obj.draw()
        for obj in self.specialObjects:
            obj.draw()    
    def renderLevelObjects(self):
        for obj in self.levelObjects:
            obj.draw()
    def addObject(self, obj,pos):
        o = None
        if(isinstance(obj, GameBox)):
            o=GameBox(self.screen, pos)
        if(isinstance(obj, GameBall)):
            if(self.hasBall):
                return
            o =GameBall(self.screen, pos, 0)
            self.hasBall=True
        if(isinstance(obj, GamePlatform)):
            o =GamePlatform(self.screen,pos)
        o.setPosition(pos)    
        self.levelObjects.append(o)
    
    def loop(self):
        while self.isLoop == 1:
            self.screen.fill((25, 95, 161))
            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.isLoop = 0
                if event.type == pygame.MOUSEBUTTONDOWN :
                    for special in self.specialObjects:
                        if(special.rect.collidepoint(event.pos)):
                            if(isinstance(special, ExitButton)):
                                pygame.quit()
                                exit()
                            if(isinstance(special, SaveButton)):
                                return self.getSaveJson()
                                
                            if(isinstance(special, DeleteButton)):
                                if(self.selectedObject!=None):
                                    self.levelObjects.remove(self.selectedObject)
                                    if(isinstance(self.selectedObject, GameBall)):
                                        self.hasBall =False
                            continue

                    isNotmenu = True
                    for mobj in self.menuObjects:
                        if(mobj.rect.collidepoint(event.pos)):
                            isNotmenu = False
                            mobj.isSelected = 1
                            self.selectedMenuObject = mobj
                        else:
                            mobj.isSelected = 0

                         
                    if(isNotmenu & (self.selectedMenuObject != None)):
                        self.addObject(self.selectedMenuObject,event.pos)
                    else:
                        if(isNotmenu):
                            if(self.selectedObject!= None):
                                self.selectedObject.setPosition(event.pos)
                                self.selectedObject.isSelected = 0
                                self.selectedObject=None
                            else:
                                for obj in self.levelObjects:
                                    if(obj.rect.collidepoint(event.pos)):
                                        obj.isSelected = 1
                                        self.selectedObject = obj
                                    else:
                                        obj.isSelected = 0
                    if(isNotmenu):
                        self.selectedMenuObject = None 

            self.renderMenu()
            self.renderLevelObjects()
            pygame.display.flip()
            self.clock.tick(FPS)
