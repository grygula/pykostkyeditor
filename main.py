import pygame, sys, random, time,json,menus
from editor import Editor
from pygame import draw, display, rect
from pygame.locals import *
from asyncore import write

try:
    import android
except ImportError:
    android = None

JSON_LOCATION='../PyKostky/data.json'
def getBoardData(dom,selected):
    boxsc=[]
    platforms=[]
    board = dom[selected]
    pad = board["pad"]
    padCords=(int(pad["x"]),int(pad["y"]))
    bxs = board["bxs"]
    for bx in bxs:
        boxsc.append((int(bx["x"]),int(bx["y"])))
    ptfms = board["platforms"]
    for ptfm in ptfms:
        platforms.append((int(ptfm["x"]),int(ptfm["y"])))
    return [boxsc,padCords,platforms]

def renderBoardsOptions(s):
    xmlbrds=getBoardsJSON()
    brds=[]
    for xmlb in xmlbrds:
        brds.append(xmlb["name"])
    selectedboard=menus.showBoardsOptions(s,brds)
    if(selectedboard==-1):
        return 1;
    loadgame(s,selectedboard)    
def getLevelCount():
    d = getBoardsJSON()
    return len(d);

def getBoardsJSON():
    data = getJson();
    xmlbrds=data["boards"]
    return xmlbrds

def getJson():
    json_data = open(JSON_LOCATION)
    data = json.load(json_data)
    return  data
      
def loadSelectedBoard(xmlbrds,selectedboard):
    d=getBoardData(xmlbrds,selectedboard)
    return d
    
def updateJson(c,v):
    json_data = open(JSON_LOCATION)
    data = json.load(json_data)
    brds=data["boards"]
    if(c>len(brds)):
        brds.append(v)
    else:
        brds[c]=v
    n =json.dumps(data)
    json_data2 = open(JSON_LOCATION,"w")
    json_data2.write(n)
    json_data2.close()
        
def loadgame(s,selectedBoard):
    d =loadSelectedBoard(getBoardsJSON(),selectedBoard)
    edit = Editor(s,d[0],d[1],d[2],selectedBoard)
    r =edit.loop()
    updateJson(selectedBoard,r)
    
def setCurrentLevel(level):
    json_data = open(JSON_LOCATION)
    data = json.load(json_data)
    configs=data["gameData"]
    configs["currentLevel"]=level
    n =json.dumps(data)
    json_data2 = open(JSON_LOCATION,"w")
    json_data2.write(n)
    json_data2.close()
            
def showMenus(s,c):
    if c==1:
        p = menus.showMenu(s)
        showMenus(s,p)
        return
    elif c==3:
        p= renderBoardsOptions(s)
        showMenus(s,p)
        return
    elif c==2:
        c = getLevelCount()
        edit = Editor(s,None,None,None,c)
        r =edit.loop()
        updateJson(c+1,r)
        showMenus(s,1)
        return
    else:
        p = menus.showMenu(s)
        showMenus(s,p)
        return
def main():
    pygame.init()
    if android:
        android.init()
        android.map_key(android.KEYCODE_BACK, pygame.K_ESCAPE)
    s = pygame.display.set_mode((800, 480))
    showMenus(s,1)
if __name__ == '__main__':
    main()

