import pygame
from pygame import draw, display, rect
from pygame.locals import *


def showMenu(s):
    titleFont = pygame.font.Font('freesansbold.ttf', 80)
    gm = titleFont.render('Edit Level', True, (255,199,20))
    gmpos =gm.get_rect(centerx=s.get_width()/2);
    ab = titleFont.render('New Level', True, (255,199,20))
    while True:
        s.fill((25, 95, 161))
        gmb = s.blit(gm, (gmpos.x,100))
        abb = s.blit(ab, (gmpos.x, 300))
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if gmb.collidepoint(pos):
                    return 3
                if abb.collidepoint(pos):
                    return 2
        pygame.display.update()


def showBoardsOptions(s,xmlbrds):
    
    font = pygame.font.Font('freesansbold.ttf', 25)
    titleFont = pygame.font.Font('freesansbold.ttf', 30)
    ab = titleFont.render('Back', True, (0,0,0))
    brds=[]
    for xmlb in xmlbrds:
        tf = font.render(xmlb, True, (255,199,20))
        brds.append(tf)
    while True:
        s.fill((25, 95, 161))
        abb = s.blit(ab, (s.get_width()-100, s.get_height()-50))
        brdsbxs=[]
        i=0
        for brd in brds:
            i=i+1
            tb=s.blit(brd, ((brd.get_rect(centerx=s.get_width()/2).x), (i*80)))
            brdsbxs.append(tb)
                
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if abb.collidepoint(pos):
                    return -1
                z=0
                for bx in brdsbxs:
                    if bx.collidepoint(pos):
                        return z
                    z=z+1
        pygame.display.update()
